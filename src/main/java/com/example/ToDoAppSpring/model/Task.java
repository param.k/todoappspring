package com.example.ToDoAppSpring.model;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Random;
import java.util.UUID;
@Getter
@Setter
public class Task {
    private String id;
    private String desc;
    private Date dateofTask;
    private Boolean isCompleted;

    public Task() {
    }

    public Task(String desc, Date dateofTask, Boolean isCompleted) {
        this.desc = desc;
        this.dateofTask = dateofTask;
        this.isCompleted = isCompleted;
    }

    public void setRandomId(String str) {
        this.id = str;
    }
    public void updateStatusCompleted(){
        this.isCompleted = true;
    }

}
