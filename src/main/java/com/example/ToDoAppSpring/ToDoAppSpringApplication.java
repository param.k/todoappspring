package com.example.ToDoAppSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToDoAppSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToDoAppSpringApplication.class, args);
	}

}
