package com.example.ToDoAppSpring.service;

import com.example.ToDoAppSpring.model.Task;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public interface TaskServiceInterface {

    public ArrayList<Task> getAllTask();  // a
    public Task addNewtask(Task task); // b

    public Map<Date,ArrayList<Task>> getTaskGroupByDate(); // b
    public void deleteTask(String taskId); // c
    public Optional<Task> updateisComplete(String taskId); // d

//    public Optional<Task> searchMatchWord() // e


}
