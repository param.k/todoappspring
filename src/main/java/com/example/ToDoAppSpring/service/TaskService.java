package com.example.ToDoAppSpring.service;

import com.example.ToDoAppSpring.model.Task;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TaskService implements TaskServiceInterface {

    public ArrayList<Task> allTasks = new ArrayList<>();

    @Override
    public ArrayList<Task> getAllTask(){
        return allTasks;
    }

    @Override
    public Task addNewtask(Task task){
        task.setRandomId(UUID.randomUUID().toString().replace("-", ""));
        allTasks.add(task);
        return task;
    }

    @Override
    public Map<Date,ArrayList<Task>> getTaskGroupByDate(){
        Map<Date,ArrayList<Task>> tmpMap = new HashMap<Date, ArrayList<Task>>();
        allTasks.forEach(p->{
            Date taskDate = p.getDateofTask();
            if(tmpMap.containsKey(taskDate)) tmpMap.get(p.getDateofTask()).add(p);
            else {
                ArrayList<Task> taskList = new ArrayList<>();
                taskList.add(p);
                tmpMap.put(taskDate, taskList);
            }
        });
            return tmpMap;
    }
    @Override
    public void deleteTask(String taskId){
        allTasks.removeIf(p->p.getId().equals(taskId));
    }
//    public ResponseEntity<Task>
    @Override
    public Optional<Task> updateisComplete(String taskId){
        allTasks.forEach(p->{
            if(p.getId().equals(taskId)){
                p.updateStatusCompleted();
            }
        });
        Optional<Task> tmp = allTasks.stream().filter(p->p.getId().equals(taskId)).findFirst();
        return tmp;
    }
}
