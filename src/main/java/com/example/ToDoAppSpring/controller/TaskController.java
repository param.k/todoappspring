package com.example.ToDoAppSpring.controller;

import com.example.ToDoAppSpring.model.Task;
import com.example.ToDoAppSpring.service.TaskService;
import com.example.ToDoAppSpring.service.TaskServiceInterface;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService t;

    @GetMapping("/getAll")
    public ResponseEntity<ArrayList<Task>> getAllTask(){
        ArrayList<Task>allTasks = t.getAllTask();
        if(!allTasks.isEmpty()) return new ResponseEntity<>(allTasks,HttpStatus.OK);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/getGroupByDate")
    public Map<Date,ArrayList<Task>> getTaskGroupByDate(){
        return t.getTaskGroupByDate();
    }

    @PostMapping
    public ResponseEntity<Task> addTask(@RequestBody Task task, HttpServletRequest request){
        t.addNewtask(task);
        return new ResponseEntity<>(task,HttpStatus.CREATED);
    }

    @DeleteMapping("/{taskId}")
    public ResponseEntity<HttpStatus> deleteTask(@PathVariable String taskId){
        t.deleteTask(taskId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{taskId}")
    public ResponseEntity<Task> updateStatus(@PathVariable String taskId){
        Optional<Task> tmp = t.updateisComplete(taskId);
        return tmp.isPresent() ? new ResponseEntity<>(tmp.get(),HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
